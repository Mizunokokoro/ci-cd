param(
    $UNITY_VERSION = "2019.1.8f1",
    $UNITY_EXECUTABLE,
    $BUILD_TARGET = "Lumin, Quest, WaveVR, PicoVR",
    $BUILD_VERSION = "0.1",
    $BUILD_VERSION_CODE = "$($ENV:CI_PIPELINE_ID)",
    $BUNDLE_VERSION_CODE = "$($ENV:CI_PIPELINE_ID)",
    $BUILD_NAME = "AppName",
    $PROJECT_PATH = ".",
    $BUILD_PATH = ".",
    $BUILD_FILE,
    $UNITY_LOGFILE = "$($PROJECT_PATH)\Logs\$($BUILD_NAME)-$($BUILD_TARGET)-logs.txt",
    $UNITY_ARGS,
    $ARTIFACTORY_USER = $ENV:ARTIFACTORY_USER, #Taken from Settings/CI/CD Environment Variables in Gitlab
    $ARTIFACTORY_PWD = $ENV:ARTIFACTORY_PASS, #Taken from Settings/CI/CD Environment Variables in Gitlab
    $UPLOADBUILD = $false,
    $KEYSTORE_PASS,
    $KEYSTORE_FILE,
    $KEY_ALIAS_PASS,
    $KEY_ALIAS_NAME
)

. ".\unitycicd\ci\common.ps1"

# Unity Build Script
# Version 2.0 Refactor common functionality
# Version 1.7 - Refactored to run once for each build
# Version 1.6 - Clean the repo in between builds so no changes from previous build cause issues as per VXR-692
# Version 1.5 - Updated to include build ID in the artifactory upload folder as per VXR-712
# Version 1.4 - Added Windows build in zip file
# Version 1.3 - Added Android keystore builds
# Version 1.2 - Added Artifactory integration to save builds outside of Gitlab
# Version 1.1 - Added Quest Builds and seperated out into switch statement and added function for checking of build errors
# Version 1.0 - Original Script for Lumin and Windows

Function Push-Artifact($Target, $BuildFile) {
    if ((! $ENV:ARTIFACTORY_USER) -or (! $ENV:ARTIFACTORY_PASS)) {
        Write-Host "Artifactory username and password are not set in Gitlab!`nGo to Settings -> CI/CD -> Environmental Variables and confirm they exist."
        exit 1
    } 
    if (Test-Path $BuildFile) {
        $uri = "https://vxr-generic-local.artifactory.eng.vmware.com/artifactory/"
        $filename = ($BuildFile).split("\")[-1]
        $path = "vxr-generic-local/builds/$($ENV:CI_PROJECT_NAME)/$($ENV:CI_COMMIT_REF_NAME)/$($ENV:CI_PIPELINE_ID)/$Target/$($filename)"
        $downloadURI = $uri + $path
        Write-Host "Uploading $BuildFile to $downloadURI"
        Invoke-Expression "& `"jfrog`" rt u $BuildFile $path --url=$uri --user=$($ENV:ARTIFACTORY_USER) --password=$($ENV:ARTIFACTORY_PASS) --build-name $BUILD_NAME --build-number=$($ENV:CI_PIPELINE_ID)" 
        Send-SlackMessage -Status "pass" -URI $downloadURI -Channel "#vxr-build"
        
        #Force adb to close after job
        Invoke-Expression "& `"C:\Users\VXR\AppData\Local\Android\Sdk\platform-tools\adb.exe`" kill-server"
    }
    else {
        Write-Host "Could not find $BuildFile to upload"
        exit 1
    }
}

Clear-UnitySmokeCaches

# Set Environmental Variables to be used by the build script
if ($KEYSTORE_PASS) {
    $env:KEYSTORE_PASS = $KEYSTORE_PASS
}
if ($KEYSTORE_FILE) {
    $env:KEYSTORE_FILE = $KEYSTORE_FILE
}
else {
    $env:KEYSTORE_FILE = "unitycicd\certs\Oculus\user.keystore"
}
if ($KEY_ALIAS_PASS) {
    $env:KEY_ALIAS_PASS = $KEY_ALIAS_PASS
}
if ($KEY_ALIAS_NAME) {
    $env:KEY_ALIAS_NAME = $KEY_ALIAS_NAME
}
else {
    $env:KEY_ALIAS_NAME = "vxr"
}

# Set Arguments
Switch -Regex ($BUILD_TARGET) {
    "Win64" {
        $BUILD_FILE = "$($BUILD_PATH)\Win64\$($BUILD_NAME)-$($BUILD_TARGET)-$BUILD_VERSION-$($ENV:CI_PIPELINE_ID).exe"
        $UNITY_ARGS = "-projectPath $($PROJECT_PATH) -logFile $($UNITY_LOGFILE) -buildTarget Win64 -executeMethod BuildPlayer.PerformWindowsBuild -buildLocation=$($BUILD_FILE) -buildVersion=$($BUILD_VERSION) -buildVersionCode=$($BUILD_VERSION_CODE) -bundleVersionCode=$($BUNDLE_VERSION_CODE) -batchmode -quit"
    }
    "Quest" {
        $BUILD_FILE = "$($BUILD_PATH)\$($BUILD_NAME)-$($BUILD_TARGET)-$BUILD_VERSION-$($ENV:CI_PIPELINE_ID).apk"
        $UNITY_ARGS = "-projectPath $($PROJECT_PATH) -logFile $($UNITY_LOGFILE) -buildTarget Android -executeMethod BuildPlayer.PerformQuestBuild -buildLocation=$($BUILD_FILE) -buildVersion=$($BUILD_VERSION) -buildVersionCode=$($BUILD_VERSION_CODE) -bundleVersionCode=$($BUNDLE_VERSION_CODE) -setDefaultPlatformTextureFormat astc -batchmode -quit"
        if ( Test-Path "$($PROJECT_PATH)\Assets\Plugins\Android\AndroidManifestForOculus.xml") {
            Write-Host "Renaming Android manifest file to use Oculus file for build"
            Rename-Item -Path "$($PROJECT_PATH)\Assets\Plugins\Android\AndroidManifest.xml" -NewName "AndroidManifest.old"
            Rename-Item -Path "$($PROJECT_PATH)\Assets\Plugins\Android\AndroidManifestForOculus.xml" -NewName "AndroidManifest.xml"
        }
    }
    "Lumin" {
        $BUILD_FILE = "$($BUILD_PATH)\$($BUILD_NAME)-$($BUILD_TARGET)-$BUILD_VERSION-$($ENV:CI_PIPELINE_ID).mpk"
        $UNITY_ARGS = "-projectPath $($PROJECT_PATH) -logFile $($UNITY_LOGFILE) -buildTarget Lumin -executeMethod BuildPlayer.PerformLuminBuild -buildLocation=$($BUILD_FILE) -buildVersion=$($BUILD_VERSION) -buildVersionCode=$($BUILD_VERSION_CODE) -batchmode -quit"
    }
    "WaveVR" {
        $BUILD_FILE = "$($BUILD_PATH)\$($BUILD_NAME)-$($BUILD_TARGET)-$BUILD_VERSION-$($ENV:CI_PIPELINE_ID).apk"
        $UNITY_ARGS = "-projectPath $($PROJECT_PATH) -logFile $($UNITY_LOGFILE) -buildTarget Android -executeMethod BuildPlayer.PerformWaveVRBuild -buildLocation=$($BUILD_FILE) -buildVersion=$($BUILD_VERSION) -buildVersionCode=$($BUILD_VERSION_CODE) -bundleVersionCode=$($BUNDLE_VERSION_CODE) -setDefaultPlatformTextureFormat astc -batchmode -quit"
        if (Test-Path "$($PROJECT_PATH)\Assets\Plugins\Android\AndroidManifestForWave.xml") {
            Write-Host "Renaming Android manifest file to use Wave file for build"
            Rename-Item -Path "$($PROJECT_PATH)\Assets\Plugins\Android\AndroidManifest.xml" -NewName "AndroidManifest.old"
            Rename-Item -Path "$($PROJECT_PATH)\Assets\Plugins\Android\AndroidManifestForWave.xml" -NewName "AndroidManifest.xml"
        }
    }
    "PicoVR" {
        $BUILD_FILE = "$($BUILD_PATH)\$($BUILD_NAME)-$($BUILD_TARGET)-$BUILD_VERSION-$($ENV:CI_PIPELINE_ID).apk"
        $UNITY_ARGS = "-projectPath $($PROJECT_PATH) -logFile $($UNITY_LOGFILE) -buildTarget Android -executeMethod BuildPlayer.PerformPicoVRBuild -buildLocation=$($BUILD_FILE) -buildVersion=$($BUILD_VERSION) -buildVersionCode=$($BUILD_VERSION_CODE) -bundleVersionCode=$($BUNDLE_VERSION_CODE) -setDefaultPlatformTextureFormat astc -batchmode -quit"
	if (Test-Path "$($PROJECT_PATH)\Assets\Plugins\Android\AndroidManifestForPico.xml") {
            Write-Host "Renaming Android manifest file to use Pico file for build"
            Rename-Item -Path "$($PROJECT_PATH)\Assets\Plugins\Android\AndroidManifest.xml" -NewName "AndroidManifest.old"
            Rename-Item -Path "$($PROJECT_PATH)\Assets\Plugins\Android\AndroidManifestForPico.xml" -NewName "AndroidManifest.xml"
        }
    }
}

# Run build
Write-Host "Build Target:$($BUILD_TARGET)`nBuild Branch:$($ENV:CI_COMMIT_REF_NAME)"

Invoke-Unity -Version $UNITY_VERSION -Executable $UNITY_EXECUTABLE -LogFile $UNITY_LOGFILE -Arguments $UNITY_ARGS

# Check build output
if (! (Test-Path $BUILD_FILE )) {
    Write-Host -ForegroundColor Red "Unity run succeeded but build file not created, see the log file at $($UNITY_LOGFILE)"
    Send-SlackMessage -Status "fail"
    exit 1
}
else {
    Write-Host "Build file created at $BUILD_FILE"
    if ($BUILD_TARGET -eq "Win64") {
        Write-Host "Compressing files at $($BUILD_PATH)\Win64\"
        $source = "$($BUILD_PATH)\Win64\"
        $destination = "$($BUILD_PATH)\$($BUILD_NAME)-$($BUILD_TARGET)-$($BUILD_VERSION)-$($ENV:CI_PIPELINE_ID).zip"
        If (Test-Path $destination) { Remove-item $destination }
        Add-Type -assembly "system.io.compression.filesystem"
        [io.compression.zipfile]::CreateFromDirectory($Source, $destination)
        $BUILD_FILE = $destination
    }
    if ($UPLOADBUILD) {
        Push-Artifact -Target $BUILD_TARGET -BuildFile $BUILD_FILE
    } 
}
